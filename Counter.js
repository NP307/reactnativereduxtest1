import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import {INC, DEC, AINC} from './actions';
import {bindActionCreators} from 'redux';

class Counter extends Component {
    inc = () => {
        this.props.Inc();
    };
    dec = () => {
        this.props.Dec();
    };
    aInc = () => {
        this.props.aInc();
    };
    render() {
        return (
            <View style={{marginTop: 100}}>
                <View>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap'}}>
                        <TouchableOpacity onPress={this.inc} style={{backgroundColor: 'red', width: '30%', height: 50, justifyContent: 'center'}}><Text style={{fontSize: 20, marginLeft: 5, color: 'white'}}>Inc</Text></TouchableOpacity>
                        <TouchableOpacity onPress={this.dec} style={{backgroundColor: 'blue', width: '30%', height: 50, justifyContent: 'center'}}><Text style={{fontSize: 20, marginLeft: 5, color: 'white'}}>Dec</Text></TouchableOpacity>
                        <TouchableOpacity onPress={this.aInc} style={{backgroundColor: 'green', width: '30%', height: 50, justifyContent: 'center'}}><Text style={{fontSize: 20, marginLeft: 5, color: 'white'}}>aInc</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={{justifyContent: 'center', flexDirection: 'row', marginTop: 100}}><Text style={{fontSize: 100, alignItems: 'center'}}>{this.props.count}</Text></View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        count: state.count,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({Inc: INC, Dec: DEC, aInc: AINC}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Counter);
