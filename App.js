import React, {Component} from 'react';
import Counter from './Counter';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {INCREMENT, DECREMENT, A_INCREMENT} from './types';
import thunk from 'redux-thunk';

const initialState = {
    count: 0,
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case INCREMENT:
            return { count: state.count + 1 };
        case DECREMENT:
            return { count: state.count - 1 };
        case A_INCREMENT:
            return { count: state.count + 5 };

        default: return state;
    }
}

const store = createStore(reducer, applyMiddleware(thunk));

const App = () => (
    <Provider store={store}>
        <Counter />
    </Provider>
);

export default App;
